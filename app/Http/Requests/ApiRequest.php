<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ApiRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function after($validator): void
    {

    }

    protected function failedValidation(Validator $validator): void
    {

        if ($validator->fails()) {
            $errorMessages = $validator->errors();
            throw new HttpResponseException(response()->json([
                'status' => 'error',
                'message' => 'VALIDATION_ERROR',
                'result' => $errorMessages
            ], 422));
        }

    }
}